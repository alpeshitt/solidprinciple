﻿using ISP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISP
{
    class Program
    {
        static void Main(string[] args)
        {
            bool operation = true;
            Console.Write("\n\n");
            Console.Write("Welcome to Xerox Machine:\n");
            Console.Write("------------------------------------------------");
            Console.Write("Choose the Option:\n");
            Console.Write("\n\n");
            Console.Write("1. Scan\n2. Copy\n3. Print\n4. Duplex Printing\n5. Fax\n6. Staple\n7. Exit");

            while (operation)
            {
                Console.Write("\nInput your choice :");
                int opt = Convert.ToInt32(Console.ReadLine());

                switch (opt)
                {
                    case 1:
                        Scanner scanner = new Scanner();
                        scanner.Scan();
                        break;

                    case 2:
                        Copy copy = new Copy();
                        copy.CopyPage();
                        break;

                    case 3:
                        Printer printer = new Printer();
                        printer.Print();
                        break;

                    case 4:
                        DuplexPrinting printing = new DuplexPrinting();
                        printing.DuplexPrintPage();
                        break;

                    case 5:
                        FaxMachine fax = new FaxMachine();
                        fax.Fax();
                        break;

                    case 6:
                        Staple staple = new Staple();
                        staple.StaplePage();
                        break;

                    default:
                        operation = false;
                        break;
                }
            }
        }
    }
}
