﻿using ISP.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISP.Models
{
    public class Printer : IScanner, IPrinter
    {
        public void Scan()
        {
            Console.WriteLine("Scan Pages\n");
        }
        public void Print()
        {
            Scan();
            Console.WriteLine("Print Pages\n");
        }

        
    }
}
