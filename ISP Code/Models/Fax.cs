﻿using ISP.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISP.Models
{
    public class FaxMachine : IScanner, IFax
    {
        public void Fax()
        {
            Console.WriteLine("Fax Pages\n");
        }

        public void Scan()
        {
            Fax();
            Console.WriteLine("Scan Pages\n");
        }
    }
}
