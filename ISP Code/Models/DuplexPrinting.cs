﻿using ISP.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISP.Models
{
    class DuplexPrinting : ICopy, IDuplexPrinting
    {
        public void CopyPage()
        {
            Console.WriteLine("Copy Both Side Pages\n");
        }
        public void DuplexPrintPage()
        {
            CopyPage();
            Console.WriteLine("Print Both Side Pages\n");

        }
    }
}
