﻿using ISP.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISP.Models
{
    public class Copy : IScanner, ICopy
    {
        public void CopyPage()
        {
            Scan();
            Console.WriteLine("Copy Pages\n");
        }

        public void Scan()
        {
            Console.WriteLine("Scan Pages\n");
        }
    }
}
