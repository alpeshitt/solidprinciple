﻿using ORMSolid.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMSolid.ContextClasses
{
   public class DBContext : IDBContext, IDisposable
    {
        public DBContext(string connectionString)
        {

        }

        public void ConnectionOpen()
        {
            throw new NotImplementedException();
        }

        public Object ExecuteQuery(string query)
        {
            throw new NotImplementedException();
        }

        public void ExecuteNonQuery(string query)
        {
            throw new NotImplementedException();
        }

        public virtual int SaveChanges()
        {
            //1 will be true and 0 will be false
            return 1;
        }

        public void ConnectionClose()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {

        }

        
    }
}
