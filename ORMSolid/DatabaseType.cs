﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMSolid
{
    public enum DatabaseType
    {
        SQLServer,
        Oracle,
        MySQL
    }
}
