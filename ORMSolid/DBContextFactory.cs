﻿using ORMSolid.ContextClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMSolid
{
    public class DBContextFactory
    {
        public static IDBContext CreateContext(DatabaseType databaseType, string connString)
        {
            if (databaseType.Equals(DatabaseType.SQLServer))
                return new SQLDBContext(connString);
            else if (databaseType.Equals(DatabaseType.SQLServer))
                return new OracleDBContext(connString);
            else
                return new MySQLDBContext(connString);

        }
    }
}
