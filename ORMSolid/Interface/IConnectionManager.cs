﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMSolid.Interface
{
    public interface IConnectionManager
    {
        IDBContext GetConnection(string connString, DatabaseType databaseType, out int identifier);
        void FreeConnection(int identifier);
    }
}
