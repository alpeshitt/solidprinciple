﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMSolid.Interface
{
    public interface IDBContext : IDisposable
    {
        void ConnectionOpen();

        Object ExecuteQuery(string query);

        void ExecuteNonQuery(string query);

        int SaveChanges();

        void ConnectionClose();
    }
}
