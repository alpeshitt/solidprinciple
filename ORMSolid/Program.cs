using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ORMSolid.ConnectionPool;
using ORMSolid.Interface;

namespace ORMSolid
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read Connection String
            string connString = ConfigurationManager.AppSettings["ConnectionString"];
            string query = string.Empty;

            //SQL Server
            using (var connectionManager = new ConnectionManager())
            {
                int iConn = 0;
                try
                {
                    using (IDBContext dbContext = connectionManager.GetConnection(connString, DatabaseType.SQLServer, out iConn))
                    {
                        query = "Insert into Users values(1, 'alpesh','choubisa','alpesh.choubisa@hp.com')";
                        dbContext.ExecuteNonQuery(query);
                    }
                }
                finally
                {
                    connectionManager.FreeConnection(iConn);
                }
            }

            //MySQL Server
            DataTable dt = new DataTable();
            using (var connectionManager = new ConnectionManager())
            {
                int iConn = 0;
                try
                {
                    using (IDBContext dbContext = connectionManager.GetConnection(connString, DatabaseType.MySQL, out iConn))
                    {
                        query = "Select * from Users";
                        dt = (DataTable) dbContext.ExecuteQuery(query);
                    }
                }
                finally
                {
                    connectionManager.FreeConnection(iConn);
                }
            }

        }
    }
}
