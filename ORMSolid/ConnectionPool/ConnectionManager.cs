﻿using ORMSolid.ContextClasses;
using ORMSolid.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ORMSolid.ConnectionPool
{
    public class ConnectionManager : IConnectionManager, IDisposable
    {
        private IDBContext[] Connections;
        private int POOL_SIZE = 100;
        private int MAX_IDLE_TIME = 10;

        private int[] Locks;
        private DateTime[] Dates;

        public IDBContext GetConnection(string connString, DatabaseType databaseType, out int identifier)
        {
            for (int i = 0; i < POOL_SIZE; i++)
            {
                if (Interlocked.CompareExchange(ref Locks[i], 1, 0) == 0)
                {
                    if (Dates[i] != DateTime.MinValue && (DateTime.Now - Dates[i]).TotalMinutes > MAX_IDLE_TIME)
                    {
                        Connections[i].Dispose();
                        Connections[i] = null;
                    }

                    if (Connections[i] == null)
                    {
                        IDBContext conn = CreateConnection(connString, databaseType);
                        Connections[i] = conn;
                        conn.ConnectionOpen();
                    }

                    Dates[i] = DateTime.Now;
                    identifier = i;
                    return Connections[i];
                }
            }

            throw new Exception("No free connections");
        }

        private IDBContext CreateConnection(string connString, DatabaseType databaseType)
        {
            if (databaseType.Equals(DatabaseType.SQLServer))
                return new SQLDBContext(connString);
            else if (databaseType.Equals(DatabaseType.SQLServer))
                return new OracleDBContext(connString);
            else
                return new MySQLDBContext(connString);
        }

        public void FreeConnection(int identifier)
        {
            if (identifier < 0 || identifier >= POOL_SIZE)
                return;

            Interlocked.Exchange(ref Locks[identifier], 0);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
