﻿using ISP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISP
{
    class PrinterWithCopy
    {
        public static void main()
        {
            bool operation = true;
            Console.Write("\n\n");
            Console.Write("Welcome to Printer with Copy:\n");
            Console.Write("------------------------------------------------");
            Console.Write("Choose the Option:\n");
            Console.Write("\n\n");
            Console.Write("1. Copy\n2. Print\n3. Exit");

            while (operation)
            {
                Console.Write("\nInput your choice :");
                int opt = Convert.ToInt32(Console.ReadLine());

                switch (opt)
                {
                    case 1:
                        Copy copy = new Copy();
                        copy.CopyPage();
                        break;

                    case 2:
                        Printer printer = new Printer();
                        printer.Print();
                        break;                   

                    default:
                        operation = false;
                        break;
                }
            }
        }
}
