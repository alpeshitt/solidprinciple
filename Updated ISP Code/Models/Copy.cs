﻿using ISP.Interfaces;
using System;

namespace ISP.Models
{
    public class Copy : ICopy
    {
        public void CopyPage()
        {
            Scan();
            Console.WriteLine("Copy Pages\n");
            Print();
        }

        public void Scan()
        {
            Console.WriteLine("Scan Pages\n");
        }
        public void Print()
        {
            Console.WriteLine("Print Pages\n");
        }
    }
}
