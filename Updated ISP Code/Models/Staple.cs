﻿using ISP.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISP.Models
{
    public class Staple : IStaple, IPrinter
    {
        public void Print()
        {
            Console.WriteLine("Print Pages\n");
        }
        public void StaplePage()
        {
            Print();
            Console.WriteLine("Staple Pages\n");
        }
    }
}
