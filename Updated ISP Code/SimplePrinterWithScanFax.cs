﻿using ISP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISP
{
    class SimplePrinterWithScanFax
    {
        public static void main()
        {
            bool operation = true;
            Console.Write("\n\n");
            Console.Write("Welcome to Simple Printer with Scan and Fax:\n");
            Console.Write("------------------------------------------------");
            Console.Write("Choose the Option:\n");
            Console.Write("\n\n");
            Console.Write("1. Scan\n2. Print\n3. Fax\n4. Exit");

            while (operation)
            {
                Console.Write("\nInput your choice :");
                int opt = Convert.ToInt32(Console.ReadLine());

                switch (opt)
                {
                    case 1:
                        Scanner scanner = new Scanner();
                        scanner.Scan();
                        break;

                    case 2:                        
                        Printer printer = new Printer();
                        printer.Print();
                        break;

                    case 3:
                        FaxMachine fax = new FaxMachine();
                        fax.Fax();
                        break;
                        
                    default:
                        operation = false;
                        break;
                }
            }
        }
    }
}
